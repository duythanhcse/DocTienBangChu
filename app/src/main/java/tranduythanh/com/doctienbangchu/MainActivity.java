package tranduythanh.com.doctienbangchu;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

import tranduythanh.com.doctienbangchu.databinding.ActivityMainBinding;
import tranduythanh.com.utils.MoneyConverter;

public class MainActivity extends AppCompatActivity {
    ActivityMainBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding=ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        adEvents();
    }
    private void adEvents() {
        binding.btnDocTien.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String docTien= MoneyConverter.TextOfMoney(binding.editNumberOfMoney.getText().toString());
                binding.txtTextOfMoney.setText(docTien);
            }
        });
    }
}